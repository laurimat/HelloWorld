﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Dog : Pet, IFavouriteAnimal
    {  
        public bool HasFleas { get; set; }

        public void Bark()
        {
            Console.WriteLine("Auh auh");
        }

        public override void Drink()
        {
            Console.WriteLine("Drinking water");
        }
    }
}
