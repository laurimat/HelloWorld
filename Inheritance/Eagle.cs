﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Eagle : Bird
    {
        public override void Fly()
        {
            Console.WriteLine("Lendan ringi");
        }

        public void HuntMouse()
        {
            Console.WriteLine("Püüan hiiri");
        }
    }
}
