﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Cat persian = new Cat();
            Cat siam = new Cat();
            Cat homeCat = siam;

            // Kaks objekti on võrdse siis, kui nad viitavad mälus samale aadressile
            // ehk kas kaks objekti on sama objekt
            if (Cat.Equals(homeCat, siam))
            {
                Console.WriteLine("Kaks objekti on võrdsed");
            }
            else
            {
                Console.WriteLine("Kaks objekti ei ole võrdsed");
            }

            int a = 5;
            int b = a;

            Cat catA = new Cat();
            Cat catB = catA;
            catB = new Cat();

            int[] numbersA = new int[] { 1, 2, 3 };

            int[] numbersB = numbersA;
           
            
            // Kui ma võrdlen lihttüüpe, siis võrreldakse nende väärtuseid
            if (a.Equals(b))
            {
                Console.WriteLine("Kaks numbrit on võrdsed");
            }
            else
            {
                Console.WriteLine("Kaks numbrit ei ole võrdsed");
            }

            string c = "Kala";
            string e = "Kala";

            if (c.Equals(e))
            {
                Console.WriteLine("Kaks stringi on võrdsed");
            }
            else
            {
                Console.WriteLine("Kaks stringi ei ole võrdsed");
            }

            var type = persian.GetType();

            Console.WriteLine("Objekt on tüübist {0}", homeCat.GetType().Name);

            persian.Name = "Miisu";
            persian.Age = 2;
            persian.Weight = 3.23;
            persian.Breed = "Persian";
            persian.LivesLeft = 7;

            persian.Drink();
            persian.Eat();
            persian.Sleep();
            persian.Purr();

            var tax = new Dog();
            tax.Name = "Muki";
            tax.Age = 2;
            tax.Weight = 1.23;
            tax.Breed = "Tax";
            tax.HasFleas = true;
            
            tax.Drink();
            tax.Eat();
            tax.Sleep();
            tax.Bark();

            List<Animal> animals = new List<Animal>();
            animals.Add(tax);
            animals.Add(persian);

            // Prindi Mina olen Cat ja minu nimi on
            // Mina olen Dog ja minu nimi on 
            foreach (var animal in animals)
            {
                Console.WriteLine("Mina olen {0} ja minu nimi on {1}", animal.GetType().Name, animal.Name);
            }

            int x = 3;
            double y = x;

            double w = 3.0;
            int z = (int)w;

            // Täpsemast üldisemaks teisendatakse automaatselt (implicit)
            // Iga koera võib võtta kui looma
            Animal newAnimal = tax;

            // Iga loom ei ole koer, teisendama pean käsitsi (explicit)
            Dog newDog = (Dog)newAnimal;
            Console.WriteLine(newDog.HasFleas);


            animals.Add(new Cat() { Name = "Kitu"  });
            animals.Add(new Dog() { Name = "Pauka" });
            // Iga looma korral animals listist kutsu vastavalt välja
            // koera puhul haukumine ja kassi puhul nurrumine/njäugumine
            foreach (var animal in animals)
            {
               // if(animal.GetType().Name == typeof(Dog).Name)
                if(animal.GetType().Name == "Dog")
                {
                    ((Dog)animal).Bark();
                }

                // is tähendab, et kontrollib, kas on konkreetset tüüpi
                // või pärineb sellest tüübist
                else if(animal is Cat)
                {
                    (animal as Cat).Purr();
                }
            }

            if(typeof(Dog) == tax.GetType())
            {

            }

            Console.ReadLine();
        }
    }
}
