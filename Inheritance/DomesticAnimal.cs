﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class DomesticAnimal : Animal
    {
        // Suunan konstruktori edasi baas klassi
        public DomesticAnimal(string name, int age) : base(name, age)
        {
            
        }


        public DomesticAnimal() : base()
        {

        }
        public override void Sleep()
        {
            Console.WriteLine("Wolf is sleeping");
        }
    }
}
