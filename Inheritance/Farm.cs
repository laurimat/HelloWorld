﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Farm : ILivingPlace
    {
        private List<Animal> animals;

        private Dictionary<string, int> animalCounts;
        private Dictionary<string, int> animalMaxCounts;

        public Farm()
        {
            animals = new List<Animal>();
            animalCounts = new Dictionary<string, int>();

            animalCounts.Add("Rabbit", 0);
            animalCounts.Add("Cow", 0);
            animalCounts.Add("Dog", 0);
            animalCounts.Add("Cat", 0);

            animalMaxCounts = new Dictionary<string, int>();

            animalMaxCounts.Add("Rabbit", 7);
            animalMaxCounts.Add("Cow", 6);
            // -1 tähendab, et lõpmatu
            animalMaxCounts.Add("Cat", -1);
            animalMaxCounts.Add("Dog", -1);
        }
        public void AddAnimal(Animal animal)
        {
            // objekt on sellest klassist või selle alamklassist
            if (animal is DomesticAnimal)
            {
               
                string animalType = animal.GetType().Name;

                if (animalMaxCounts.ContainsKey(animalType))
                {
                    int maxCount = animalMaxCounts[animalType];
                    int currentCount = animalCounts[animalType];
                    if (maxCount > currentCount || maxCount == -1)
                    {
                        animals.Add(animal);
                        animalCounts[animalType]++;

                        int freeSpaceCount = maxCount - animalCounts[animalType];
                        if(freeSpaceCount < 0)
                        {
                            freeSpaceCount = -1;
                        }
                        Console.WriteLine("Lisati loom {0}, nüüd on farmis {1}, mahub veel {2}",
                            animalType, animalCounts[animalType], freeSpaceCount);

                    }
                    else
                    {
                        Console.WriteLine("VIGA: Pole rohkem ruumi {0} jaoks", animalType);
                    }
                }
                else
                {
                    Console.WriteLine("VIGA: Farmi ei saa lisada kodulooma {0}", animalType);
                }
              

            }
            else
            {
                Console.WriteLine("VIGA: Farmi saab lisada ainult koduloomi");
            }
        }

        public int GetAnimalCount()
        {
            // Käime läbi kogu dictionary key-value paarid
            // nii, et iga paari x korral summeeri see, 
            // mis on paramalpool =>

            return animals.Count;//animalCounts.Sum(x => x.Value);
        }

        public int GetAnimalCount(string animalType)
        {
            return animalCounts[animalType];
        }

        public void RemoveAnimal(Animal animal)
        {
            string animalType = animal.GetType().Name;

            if (animalCounts.ContainsKey(animalType) && animalCounts[animalType] > 0)
            {
                animals.Remove(animal);
                animalCounts[animalType]--;
            }
            

        }
    }
}
