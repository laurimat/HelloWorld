﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public abstract class Bird
    {
        // Saab kirjelda meetodi 
        // struktuuri, mida tagastab ning mis on tema parameetrid
        // aga sisu ei lisata
        // Seda saab teha lisades ette sõna abstract

        public abstract void Fly();

        // Luues meetodi koos sisuga
        public void LayEggs()
        {
            Console.WriteLine("Munen mune");
        }
    }
}
