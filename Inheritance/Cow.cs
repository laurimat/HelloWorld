﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Cow : FarmAnimal, IWalkOnFourFeet
    {
        public void WalkOnFourFeet()
        {
            Console.WriteLine("Cow is walking on four feet");
        }
    }
}
