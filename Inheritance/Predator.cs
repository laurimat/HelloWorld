﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Predator : WildAnimal, IHunter
    {
        private string lastPrey;

        public int MaxSpeed { get; set; }

        public string GetLastPrey()
        {
            return lastPrey;
        }

        public void Hunt(string prey)
        {
            Console.WriteLine("Hunting {0}", prey);
            lastPrey = prey;
        }
        public override void Sleep()
        {
            base.Sleep();
        }
    }
}
