﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class WildAnimal : Animal
    {

        public override void Sleep()
        {
            base.Sleep();
            //Console.WriteLine("Wildanimal is sleeping");
        }
    }
}
