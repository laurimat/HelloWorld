﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    // Kujuta ette ühised omadused
    // Metsal, loomaaial ja farm
    public interface ILivingPlace
    {
        int GetAnimalCount();

        // animalType: "Cat", "Dog", "Fox"
        int GetAnimalCount(string animalType);

        void AddAnimal(Animal animal);
        void RemoveAnimal(Animal animal);
    }
}
