﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Wolf : Predator, IWalkOnFourFeet
    {  
        public override void Sleep()
        {
            base.Sleep();
          //  Console.WriteLine("Wolf is sleeping");
        }

        public void WalkOnFourFeet()
        {
            Console.WriteLine("Wolf is walking on four feet");
        }
    }
}
