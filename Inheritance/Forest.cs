﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Forest : ILivingPlace
    {
        private List<Animal> animals;
        private Dictionary<string, int> animalCounts;

        public Forest()
        {
            animals = new List<Animal>();
            animalCounts = new Dictionary<string, int>();
        }

        public void AddAnimal(Animal animal)
        {
            animals.Add(animal);
            
            string animalType = animal.GetType().Name;

            if (!animalCounts.ContainsKey(animalType))
            {
                animalCounts.Add(animalType, 1);
            }
            else
            {
                animalCounts[animalType]++;
            }

            Console.WriteLine("Lisati loom {0}, nüüd on metsas {1}",
                           animalType, animalCounts[animalType]);
        }

        public int GetAnimalCount()
        { 
            return animalCounts.Sum(x => x.Value);
        }

        public int GetAnimalCount(string animalType)
        {
            return animalCounts[animalType];
        }

        public void RemoveAnimal(Animal animal)
        {
            animals.Remove(animal);
        }
    }
}
