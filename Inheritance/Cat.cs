﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    // Internal tähendab, et ligipääs on faili piires, milles siis kood asub
    // Iga projekt on üks eraldi fail
    // Internal on vaikeväärtus klassidel

    // Private klassi ees, tähendab, et klass on nähtav ainult väljaspool oleva klassi sees
    // Tavalisele klassile, mis asub otse nimeruumi (namespace) sees, ma ei saa private kasutada

    public class Cat : Pet, IWalkOnFourFeet
    {
        public int LivesLeft { get; set; }


        public Cat(string name, int age, bool livesWithHumans, int livesLeft) : base(name, age, livesWithHumans)
        {
            LivesLeft = livesLeft;
        }


        public Cat() : base()
        {

        }

        public void Purr()
        {
            Console.WriteLine("Nurr nurr");
            CatHobby hobby = new CatHobby();
            hobby.CatchMouse();
            Walk();
            
        }

        public override void Eat()
        {
            // base tähendab baasklassi
            // Kutsub vanemklassi Eat() meetodi välja
            Console.WriteLine("Happily");
            base.Eat();
            Console.WriteLine("Mouse");
        }

        public override void Drink()
        {
            Console.WriteLine("Drinking milk");
        }

        public void WalkOnFourFeet()
        {
            Console.WriteLine("Cat is walking on four feet");
        }


        // Nested class
        // default on private
        class CatHobby 
        {
            public void CatchMouse()
            {

            }
        }
    }
}
