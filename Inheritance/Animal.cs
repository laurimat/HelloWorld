﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Animal
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }
        public string Breed { get; set; }

        public Animal(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public Animal()
        {

        }

        // Sõna virtual tähendab, et antud meetotit saab pärinevates klassides
        // üle kirjutada
        public virtual void Eat()
        {
            Console.WriteLine("Eating");
        }
        public virtual void Drink()
        {
            Console.WriteLine("Drinking");
        }
        public virtual void Sleep()
        {
            Console.WriteLine("Sleeping");
        }

        protected void Walk()
        {
           
        }
    }
}
