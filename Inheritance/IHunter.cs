﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public interface IHunter
    {
        int MaxSpeed { get; }
        void Hunt(string prey);
        string GetLastPrey();
    }
}
