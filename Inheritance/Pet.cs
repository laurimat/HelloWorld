﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Pet : DomesticAnimal
    {
        public bool LivesWithHumans { get; set; }

        public Pet(string name, int age, bool livesWithHumans) : base(name, age)
        {
            LivesWithHumans = livesWithHumans;
        }


        public Pet() : base()
        {

        }
    }
}
