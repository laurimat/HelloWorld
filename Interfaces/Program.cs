﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Wolf wolf = new Wolf();
            wolf.Hunt("Rat");

            Console.WriteLine("Viimane saakloom oli {0}", wolf.GetLastPrey());

            Fox fox = new Fox();
            fox.Hunt("Rabbit");

            Cow cow = new Cow();
            cow.WalkOnFourFeet();
            wolf.WalkOnFourFeet();

            IWalkOnFourFeet firstFourFeetWalker = cow;
            IWalkOnFourFeet secondFourFeetWalker = wolf;

            List<IWalkOnFourFeet> fourFeetWalkers = new List<IWalkOnFourFeet>();
            fourFeetWalkers.Add(cow);
            fourFeetWalkers.Add(wolf);
            fourFeetWalkers.Add(new Cat());

            List<IFavouriteAnimal> favourites = new List<IFavouriteAnimal>();
            favourites.Add(new Dog());
            favourites.Add(new Dog());
            favourites.Add(new Dog());
            favourites.Add(new Rabbit());
            favourites.Add(new Fox());

            var favouriteDog = favourites[2];
            var dog = (Dog)favouriteDog;
            dog.Name = "Pauka";

            Console.ReadLine();
        }
    }
}
