﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            bool doContinue = true;
            string answer;

            Program program = new Program();

            while(doContinue)
            {
                Console.WriteLine("Sisesta kaks numbrit, eralda tühiku, koma või tabiga");
                string row = Console.ReadLine();

                // new char[] { ' ' } tähendab, et luuakse char massiiv ning pannakse sisse üks element
                // selleks on ' ' ehk tühik
                // annab võimaluse anda Split meetodi argumendina kaasa mitu erinevat karakterit
                // mille pealt soovime stringi tükeldada.
                // StringSplitOptions.RemoveEmptyEntries on seadistus, mis eemaldab tühjad stringid massivist
                string[] words = row.Split(new char[] { ' ', ',', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                //int a = Convert.ToInt32(words[0]);
                //int a = int.Parse(words[0]);
                if (words.Length == 2)
                {
                    int a;
                    bool firstWordIsNumber = int.TryParse(words[0], out a);
                    int b;
                    bool secondWordIsNumber = int.TryParse(words[1], out b);
                    if (firstWordIsNumber && secondWordIsNumber)
                    {
                        bool doContinueCalculate = true;
                        while(doContinueCalculate)
                        {
                            program.PrintOperationSelections();

                            string operation = Console.ReadLine();

                            char operationChar;
                            bool operationIsChar = char.TryParse(operation, out operationChar);
                            if (operationIsChar)
                            {
                                program.Calculate(a, b, operationChar);
                            }
                            else
                            {
                                Console.WriteLine("VIGA. Sisetama peab üksiku sümboli");
                            }
                            Console.WriteLine("Kas tahad samade numbritega veel arvutada? Vasta y/n");
                            answer = Console.ReadLine();

                            if (answer != "y")
                            {
                                doContinueCalculate = false;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("VIGA. Ei sisestatud korrekseid kahte numbrit");
                    }
                }
                else
                {
                    Console.WriteLine("VIGA. Sisetama peab kaks tehte liiget");
                }

                Console.WriteLine("Kas tahad veel arvutada? Vasta y/n");
                answer = Console.ReadLine();

                if(answer != "y")
                {
                    doContinue = false;
                }
               
            }       
        }

        private void Calculate(int a, int b, char operationChar)
        {
            if (operationChar == 'a')
            {
                Console.WriteLine("Arvude {0} ja {1} summa on: {2}", a, b, Sum(a, b));
            }
            else if (operationChar == 'b')
            {
                Console.WriteLine("Arvude {0} ja {1} vahe on: {2}", a, b, Subtract(a, b));
            }
            else if (operationChar == 'c')
            {
                Console.WriteLine("Arvude {0} ja {1} korrutis on: {2}", a, b, Multiply(a, b));
            }
            else if (operationChar == 'd')
            {
                Console.WriteLine("Arvude {0} ja {1} jagatis on: {2}", a, b, Divide(a, b));
            }
            else
            {
                Console.WriteLine("VIGA. Sisetama peab a, b, c või d");
            }
        }

        private void PrintOperationSelections()
        {
            Console.WriteLine("Vali tehe, sisestades a, b, c või d.");
            Console.WriteLine("a) Liida");
            Console.WriteLine("b) Lahuta");
            Console.WriteLine("c) Korruta");
            Console.WriteLine("d) Jaga");
            Console.WriteLine("e) Leia kumb on suurem");
        }

        static int Sum(int a, int b)
        {
            var sum = a + b;
            return sum;
        }

        int Subtract(int a, int b)
        {   
            return a - b;
        }

        int Multiply(int a, int b)
        {
            return a * b;
        }

        double Divide(double a, double b)
        {
            return a / b;
        }

        int Increase(int number, int step)
        {
            // number = number + step
            number += step;
            return number;
        }
    }
}
