﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        
        static void Main(string[] args)
        {
            
            SayHello();
            Sum(2, -1);
            SayHello();
            SayHello();
            Sum(5, 10);            
            SayHello();

            SayHello("Lauri");

          

            SayHello("Pille");
           
            SayHello("Malle");
            SayHello("Tiiu");
            
            // Loo meetod, mis ei tagasta midagi vaid prindib ekraanile Hello nimi.
            // Meetodi parameetriks on nimi
            // Meetodi nimeks sama, SayHello

            // Loo meetod, mis ütleb sellise lause "Tere minu nimi on 'name' ja ma olen 'age' aastane"
            // SayNameAndAge
            // Parameetrid ongi nimi ja vanus
            SayNameAndAge("Kalle", 12);
                       
            Console.ReadLine();
        }

        // void tähendab, et see meetod ei tagasta midagi
        static void SayHello()
        {
            Console.WriteLine("Hello");
        }

        // Nimetatakse Method Overloading ehk meetodi ülelaadimine
        static void SayHello(string name)
        {
            Console.WriteLine("Hello {0}", name);
        }
        

        static void SayNameAndAge(string name, int age)
        {
            Console.WriteLine("Tere minu nimi on {0} ja ma olen {1} aastane", name, age);
        }

        static void Sum(int a, int b)
        {           
            int sum;
            sum = a + b;
            Console.WriteLine("Arvude {0} ja {1} summa on {2}", a, b, sum);
        }

    }
}
