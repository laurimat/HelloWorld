﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doubles
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            double c;
            double d;

            a = 2.54;
            b = -124.53241;

            c = a * b;
            d = c / 56.324;
            Console.WriteLine(c.ToString(new CultureInfo("en-US")));
            Console.WriteLine(d);

            d = -5.61594257442653;
            Console.WriteLine(d);

            // Teisendamisel täpsemast tüübist vähem täpsemasse
            // lõigatakse numbril tagant ära osa, mis ei mahu
            // ilma ümardamata
            float e = (float)d;
            Console.WriteLine(e);

            // Implicit teisendamine
            d = e;

            int f = 4;

            // Implicit teisendamine täisarvust reaaravu
            double g = f;

            double h = 3.76;
            // Explicit, lõigatakse kõik maha, mis on peale koma
            f = (int)h;
            Console.WriteLine(f);

            Console.ReadLine();
        }
    }
}
