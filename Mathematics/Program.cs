﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathematics
{
    class Program
    {
        static void Main(string[] args)
        {
            int abs = Math.Abs(-234);
            Console.WriteLine(abs);

            var a = Math.Abs(-53.2633f);

            // Implicitly-typed variable
            var b = "Tere";
            var c = 3;
            var e = 3.634673;
                        
            // int = Int32, long = Int64
            var f = -30000000000;
            // float = Single
            var g = 235.34f;

            int i = 3;
            Int32 j = 3;
            var k = 3;

            int l;
            l = 3;

            // Char karakter/sümbol
            // Koosneb alati ainult ühest sümbolist

            char o = '#';
            var m = 'a';
            var n = ' '; 
            var p = '\n';
            var q = '3';
            var r = "";

            //Boolean bool = Boolean
            bool s = false;
            s = true;
            var t = true;

            Console.WriteLine(Math.Ceiling(2.0000001));
            Console.WriteLine(Math.Floor(2.9999999));
            Console.WriteLine(Math.Round(2.14372362362, 2));

            // Kas arv on paaris?
            var u = 32;
            // Jagamisel jäägi leidmine
            var reminder = u % 2;
            Console.WriteLine(reminder);
            // Kas jääk on võrdne 0'ga?
            if (reminder == 0)
            {
                Console.WriteLine("Arv {0} on paaris", u);
            }
            // Muul juhul (ehk siis paaritu arv)
            else
            {
                Console.WriteLine("Arv {0} on paaritu", u);
            }

            Console.ReadLine();

        }
    }
}
