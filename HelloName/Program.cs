﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloName
{
    class Program
    {
        static void Main(string[] args)
        {
            // Deklareerime muutuja koos väärtusega
            // camelcase
            // meetodi sisesed (privaatmuutuja)
            // muutujad on väikse algustähega
            string firstName = "Lauri";

            // Deklareerime muutuja
            string lastName;
            // Anname muutujale väärtuse
            lastName = "Mattus";
            // Stringide liitmine + märgiga
            Console.WriteLine("Hello " + firstName + " " + lastName);

            string petName = "Muri";

            Console.WriteLine("Hello {0} {1} where is {2}?", firstName, lastName, petName);

            // Muutujale uue väärtuse andmine
            petName = "Pauka";

            Console.WriteLine("Hello {0} {1} where is {2}?", firstName, lastName, petName);


            string fullName = string.Format("{0} {1}", firstName, lastName);

            Console.WriteLine("Hello " + fullName);

            // Stringi sees olevad \-ga algavad sümbolid/tähed tähendavad käsku
            // nimetatakse escape sequences
            // \n reavahetus
            // \ kasutatakse ka eri sümbolite teksi sees kuvamiseks
            // \\n tühistab \n ära ja näitab teksist \n
            string song = "Elas metsas \"Mutionu\",\nkeset kuuski noori vanu.";
            Console.WriteLine(song);

            Console.ReadLine();
        }
    }
}
