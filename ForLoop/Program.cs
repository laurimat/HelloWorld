﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // For tsükli tingimus
            // koosneb kolmest osast,
            // mille vahel on semikoolon ;

            // 1 element - tegevus, mis tehakse enne tsüki käivitamist
            // 2 element - tingimus, mis peab tsükli vältel oleme tõene
            // 3 element - tegevus, mis iga tsüki korduse lõpus tehakse
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(i);
            }

            int a = 0;
            while(a < 5)
            {
                Console.WriteLine(a);
                a++;
            }

            Console.WriteLine();

            for (int i = 4; i < 15; i++)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();
            for (int i = 14; i >= 4;i--)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine();
            // Prindi välja iga teine number 1 ja 11 vahel

            for (int i = 2; i < 11; i += 2)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();
            
            for (int i=10, j=1; j < 11 ;i--, j++)
            {
                //Console.WriteLine("{0}\n{1}", i, j);
                Console.WriteLine(i);
                Console.WriteLine(j);
            }

            Console.WriteLine();

            int b = 10;
            int c = 1;
            while (b > 0)
            {
                Console.WriteLine(b);
                Console.WriteLine(c);
                b--;
                c++;
            }


          


            Console.ReadLine();
        }
    }
}
