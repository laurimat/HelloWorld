﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace FarmExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ILivingPlace livingPlace = new Forest();

            Wolf wolf = new Wolf();
            livingPlace.AddAnimal(wolf);
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Cat());
            livingPlace.AddAnimal(new Dog());
            livingPlace.AddAnimal(new Cow());

            Console.WriteLine("Kasse on mul {0} tükki", livingPlace.GetAnimalCount("Cat"));

            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());
            livingPlace.AddAnimal(new Rabbit());

            Console.WriteLine("Kokku on mul loomi {0} tükki", livingPlace.GetAnimalCount());

            Bird bird = new Eagle();
            bird.Fly();
            bird.LayEggs();
            
            Eagle eagle = new Eagle();
            eagle.Fly();
            bird.LayEggs();

            Console.ReadLine();
        }
    }
}
