﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReadFromFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // Kontrollin enne, kas selline fail on olemas
            if (File.Exists("input.txt"))
            {
                // FileMode.OpenOrCreate kui faili pole, loob selle, kui on siis avab
                FileStream stream = new FileStream("input.txt", FileMode.Open, FileAccess.Read);

                // Encoding peab olema sama, mis failiderdaktoris, millega fail on loodud
                // Encoding.Default on ANSI encoding
                StreamReader reader = new StreamReader(stream, Encoding.Default);

                string line = reader.ReadLine();
                while (line != null)
                {
                    Console.WriteLine(line);
                    line = reader.ReadLine();
                }

                //string line;

                //do
                //{
                //    line = reader.ReadLine();
                //    if(line != null)
                //    {
                //        Console.WriteLine(line);
                //    }
                  
                //    //    line = reader.ReadLine();
                //} while (line != null);

                reader.Close();
                stream.Close();
                Console.WriteLine();

                // Loeb failist kõik read ja moodustab neist ühe stringi
                // koos reavahetustega
                string text = File.ReadAllText("input.txt", Encoding.Default);
                Console.WriteLine(text);

                Console.WriteLine();

                // Loeb failist kõik read ja moodustab ridadest stringi massiivi
                string[] words = File.ReadAllLines("input.txt", Encoding.Default);
                
                for (int i = 0; i < words.Length; i++)
                {
                    Console.WriteLine(words[i]);
                }

                Console.WriteLine("Texti muutmine stringi massiiviks");
                Console.WriteLine();

                // Environment.NewLine vaatab mis keskkonnas me oleme ning kasutab siis kas \n või \r\n

                string[] textLines = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                foreach (var textLine in textLines)
                {
                    Console.WriteLine(textLine);
                }
                
                string allText = string.Join(Environment.NewLine, textLines);
                
                Console.WriteLine();
                Console.WriteLine("Stringi massiivist uuesti tekstiks");
                Console.WriteLine();

                Console.WriteLine(allText);
            }

            Console.ReadLine();
        }
    }
}
