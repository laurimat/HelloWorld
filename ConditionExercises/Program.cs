﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            // && JA Shift + 6
            // || VÕI Alt Gr + <

            // Küsime kasutajalt numbri
            //1. Kui arv on 3 ja 7 vahel, prindi "Arv on 3 ja 7 vahel"
            // Kui arv on 0 ja 3 vahel->
                //kontrolli kas arv on paaris ning prindi sellekohane lause,
                // Kui paaritu, siis sellekohane lause
            // Muul juhul prindi "Pole ükski eelnev"
       
            Console.WriteLine("Sisesta number");
            var number = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Ülesanne 1");
            if (number > 3 && number <7)
            {
                Console.WriteLine("Arv on suurem kui 3 ja ja väiksem kui 7");
            }
            else if (number > 0 && number < 3)
            {
               
                if (number % 2 == 0)
                {
                    Console.WriteLine("Arv on paaris");
                }
                else
                {
                    Console.WriteLine("Arv on paaritu");
                }
            }
            else
            {
                Console.WriteLine("Pole ükski eelnev");
            }
          

            Console.WriteLine();

            Console.WriteLine("Ülesanne 2");

            //2. Kui arv on väiksem kui 2 või suurem kui 4, prindi teade ja kontrolli
                // kas arv on väiksem kui 3, prindi teade
                // kas arv on võrdne 4 või võrdne 5, prindi teade
                // muul juhul kontrolli kas arv jagub 3'ga ja prindi teade
            // Muul juhul prindi pole ükski eelnev
            if(number < 2 || number > 4)
            {
                Console.WriteLine("Arv on väiksem kui 2 või suurem kui 4");

                var isOther = false;


                if(number < 3)
                {
                    Console.WriteLine("Arv on suurem kui 3");
                    isOther = true;
                }
                if(number == 4 || number == 5)
                {
                    Console.WriteLine("Arv on võrdne 4'ga või arve on võrdne 5'ga");
                    isOther = false;
                }

                if(isOther != true)
                {
                    if(number % 3 == 0)
                    {
                        Console.WriteLine("Arv jagub 3'ga");
                    }
                }


            }
            else
            {
                Console.WriteLine("Pole ükski eelnev");
            }
            //3. kas arv on väiksem kui 3, prindi teade
            // kas arv on võrdne 4 või võrdne 5, prindi teade
            // Kui eelnevad kaks vastasid tõele, kontrolli kas arv jagub 3'ga ja prindi teade

            Console.WriteLine();
            Console.WriteLine("Ülesanne 3");

            int counter = 0;

            if (number < 7)
            {
                Console.WriteLine("Arv on väiksem kui 7");
                counter = counter + 1;
                // counter++ suurendab isennast ühe võrra
                // counter-- vähendab iseennast ühe võrra, counter = counter - 1
                // counter += 2 suurendab iseennast kahe võrra, counter = counter + 2
                // counter -= 5 vähendab iseennast 5 võrra
                // counter *=2 korrutab iseennast 2ga, counter = counter * 2
                // counter /=3 jagab iseennast 3ga
                // counter++;
            }
            if (number == 4 || number == 6)
            {
                Console.WriteLine("Arv on võrdne 4'ga või arve on võrdne 6'ga");
                counter++;
            }
            if (counter == 2 && number % 3 == 0)
            {              
                Console.WriteLine("Arv jagub 3'ga");
            }

           

            Console.ReadLine();
        }
    }
}
