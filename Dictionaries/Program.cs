﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionaries
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            // Key on alati unikaalne, kui eksisteeriva key'ga lisada uus element,
            // annab vea
            // Igal lisamisel võiks kontrollida, kas sellise key'ga juba eksisteerib
            if(!dictionary.ContainsKey("Auto"))
            {
                dictionary.Add("Auto", "Car");
            }
            
            dictionary.Add("Maja", "House");
            dictionary.Add("Kass", "Cat");
            dictionary.Add("Puu", "Tree");
            dictionary.Add("Komm", "Candy");

            foreach (var item in dictionary)
            {
                Console.WriteLine("Key: {0}, Value: {1}", item.Key, item.Value);
            }


            Console.WriteLine("Kass on inglise keeles {0}", dictionary["Kass"]);
            // Kui tahan kustutada sõna Maja koos tõlkega
            // Ei anna viga, kui elementi pole, lihtsalt ignoreerib
            dictionary.Remove("Maja");

            // kui tahan kõiki key'si "Kass", "Puu", "Komm"
            List<string> keys = dictionary.Keys.ToList();

            //"Cat", "Tree", "Candy"
            List<string> values = dictionary.Values.ToList();

            foreach (var value in values)
            {
                Console.WriteLine(value);
            }

            Dictionary<string, string> countryCodes = new Dictionary<string, string>();
            countryCodes.Add("Estonia", "EST");
            countryCodes.Add("Germany", "GER");

            Console.WriteLine("Saksamaa lühike kood on {0}", countryCodes["Germany"]);

            countryCodes["Estonia"] = "EE";
            
            Dictionary<char, int> charCounts = new Dictionary<char, int>();

            string sentence = "Elas metsas mutionu";

            //for (int i = 0; i < sentence.Length; i++)
            //{
            //    if(!charCounts.ContainsKey(sentence[i]))
            //    {
            //        charCounts.Add(sentence[i], 1);
            //    }
            //    else
            //    {
            //        charCounts[sentence[i]]++;
            //    }
            //}

            foreach (var item in sentence.ToLower())
            {
                if (!charCounts.ContainsKey(item))
                {
                    charCounts.Add(item, 1);
                }
                else
                {
                    charCounts[item]++;
                }
            }


            foreach (var item in charCounts)
            {
                Console.WriteLine("{0} {1}", item.Key, item.Value);
            }

            // Loe lauses kokku kõik erinevad tähed ja prindi välja mitu neid on
            // Elas metsas mutionu
            // E 1
            // l 1
            // a 2





            Console.ReadLine();
        }
    }
}
