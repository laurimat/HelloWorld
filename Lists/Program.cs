﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbersArray = new int[3];
            Console.WriteLine("Elementide arv massiivis on {0}", numbersArray.Length);
            // Listi loomine ei pane piiranguid elementide arvule
            // elementide arv on dünaamiline
            List<int> numbersList = new List<int>();

            Console.WriteLine("Elementide arv listis on {0}", numbersList.Count);

            numbersArray[0] = 7;
            numbersArray[1] = -7;
            numbersArray[2] = -12;

            foreach (var someNumber in numbersArray)
            {
                Console.WriteLine(someNumber);
            }

            // Lisab elendi listi lõppu
            numbersList.Add(7);

            Console.WriteLine("Elementide arv massiivis on {0}", numbersArray.Length);
            Console.WriteLine("Elementide arv listis on {0}", numbersList.Count);

            numbersArray[0] = 5;

            // Lisab elemendi kohale 0, olemasolevad elmendid nihkuvad ühe võrra edasi
            numbersList.Insert(0, 5); // 5 7
            numbersList.Insert(1, 3); // 5 3 7

            // Kui ma tahan, et esimene element oleks number 1, listis: 1 3 7

            // Kustutab elemendi indeksiga 0
            numbersList.RemoveAt(0); // 3 7
            // Lisab elemendi kohale 0, olemasolevad elmendid nihkuvad ühe võrra edasi
            numbersList.Insert(0, 1); // 1 3 7

            numbersList[0] = 5; // 5 3 7

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            // Indeksiga lisada uusi elemente listi ei saa,
            // saab ainult asendada olemasolevatel indeksitel paiknevaid elemnte
            numbersList[2] = 4; // 5 3 4

            Console.WriteLine();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            // Saan lisada nii massiivi kui listi
            numbersList.AddRange(numbersArray);

            Console.WriteLine();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.AddRange(numbersList);

            Console.WriteLine();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            // Kas listis on olemas number -2
            if(numbersList.Contains(-2))
            {
                Console.WriteLine("Number -2 on listis");
            }
            else
            {
                Console.WriteLine("Number -2 ei ole listis");
            }

            List<int> firstTwoNumbersList = numbersList.GetRange(0, 2);

            Console.WriteLine();

            for (int i = 0; i < firstTwoNumbersList.Count; i++)
            {
                Console.WriteLine(firstTwoNumbersList[i]);
            }

            int index = numbersList.IndexOf(-12);
            Console.WriteLine("Number -12 asub kohal {0} ", numbersList.IndexOf(-12, index + 1));

            // Otsib numbrit -12, seni kuni leiab, kustutab

            while (numbersList.IndexOf(-12) != -1)
            {
                numbersList.Remove(-12);
            }
            
            Console.WriteLine();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.Reverse();

            Console.WriteLine();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.Sort();

            Console.WriteLine();

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            Console.WriteLine();

            // Läbi kõik elemendid listis nii, et 
            // iga tsükli korduse jooksul on int number järgmine element
            // kasutatakse, kui on vaja läbi käia kõik listi/massiivi elemendid
            
            foreach (var number in numbersList)
            {   
                Console.WriteLine(number);
            }

            int[] newArray = numbersList.ToArray();

            List<string> stringList = new List<string>() { "Esmaspäev", "Teisipäev" };

          
            
          
            Console.ReadLine();
        }
    }
}
