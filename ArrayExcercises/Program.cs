﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[5];

            numbers[0] = 3;
            numbers[1] = -13;
            numbers[2] = 10;
            numbers[3] = 12;
            numbers[4] = -5;
            
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine();

            for (int i = 4; i >= 0; i--)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine();

            // Suurenda iga teist numbrit 3 võrra
            for (int i = 1; i < 5; i += 2)
            {
                numbers[i] += 3;
            }
                      
            Console.WriteLine();

       
            for (int i = 0; i < 5; ++i)
            {
                Console.WriteLine(numbers[i]);
            }


            // Liida kõik massivi elemendid kokku ja prindi summa välja

            
            int sum = 0;
                       
            for (int i = 0; i < 5; i++)
            {
                //sum = sum + numbers[i];
                sum += numbers[i];
            }

            Console.WriteLine("Arvude summa on {0}", sum);

            Console.WriteLine();
            int a = 5;
            int b = 5;
           
            Console.WriteLine(a++);
            Console.WriteLine(++b);

            Console.WriteLine();
            
            Console.WriteLine(a);
            Console.WriteLine(b);

            Console.WriteLine();

            int c = 1;
            int d = 1;
            int e;
            int f;
            e = c++;
            f = ++d;
            
            Console.WriteLine(e);
            Console.WriteLine(f);
            Console.WriteLine();

            Console.WriteLine(c);
            Console.WriteLine(d);

            Console.WriteLine();

            // Printida välja antud massiivist kõik 2ga jaguvad numbrid
            for (int i = 0; i < 5; i++)
            {
                if(numbers[i] % 2 == 0)
                {
                    Console.WriteLine(numbers[i]);
                }     
            }


            // Tee uus massiiv 3-le täisarvule ning pane sinna sisse esimese massivi
            // esimesed 3 numbrit
            int[] numbersSecond = new int[3];
            for (int i = 0; i < 3; i++)
            {
                numbersSecond[i] = numbers[i];

            }

            Console.WriteLine("Massiv 3 elendiga");
            Console.WriteLine();

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(numbersSecond[i]);
            }
            // Tee uus massiiv 5-le täisarvule ning pane esimese masiivi kõik numbrid
            // sinna vastupidises järjekorras
            int[] numbersThird = new int[5];
            for (int i = 0, j = 4; i < 5 && j >= 0; i++, j--)
            {
                numbersThird[i] = numbers[j];
            }

            //for (int i = 0; i < 5; i++)
            //{
            //    numbersThird[i] = numbers[4-i];
            //}

            //numbersThird[0] = numbers[4];
            //numbersThird[1] = numbers[3];
            //numbersThird[2] = numbers[2];
            //numbersThird[3] = numbers[1];
            //numbersThird[4] = numbers[0];

            //numbersThird[4] = numbers[0];
            //numbersThird[3] = numbers[1];
            //numbersThird[2] = numbers[2];
            //numbersThird[1] = numbers[3];
            //numbersThird[0] = numbers[4];

            Console.WriteLine("Massiv vastupidises järjekorras");
            Console.WriteLine();

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(numbersThird[i]);
            }

            int max = int.MinValue;

            // Leia massivist kõige suurem number ja prindi välja
            for (int i = 0; i < 5; i++)
            {
                if(numbers[i] > max)
                {
                    max = numbers[i];
                    Console.WriteLine("Uus vahepealne maksimum on {0}", max);
                }
            }

            Console.WriteLine("Maksimum on {0}", max);

            int min = int.MaxValue;

            // Leia massivist kõige väiksem number ja prindi välja
            for (int i = 0; i < 5; i++)
            {
                if (numbers[i] < min)
                {
                    min = numbers[i];                
                }
            }

            Console.WriteLine("Miinimum on {0}", min);

            Console.ReadLine();
        }
    }
}
