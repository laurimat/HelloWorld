﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = new Person();
           
            student.Introduce();
            student.Weight = 56.45;
            student.Salary = 10000;

            Console.WriteLine("Õpilase sünniaasta on {0}", student.BirthYear);
            student.SetAge(26);
            Console.WriteLine("Õpilase sünniaasta on {0}", student.BirthYear);

            Console.WriteLine("Õpilase kaal on {0}", student.Weight);
            
            student.SetFirstName("Peeter");
            student.LastName = "Kaalikas";
            student.Gender = Gender.Female;
            student.HairColor = HairColor.Grey;

            Console.WriteLine("Täisnimi on {0}", student.FullName);

            var age = student.GetAge();
           
            if (age != null)
            {
                Console.WriteLine(age);
            }
            
            student.SetAge(0);

            student.Introduce();

            var teacher = new Person();
            teacher.SetFirstName("Malle");
            
            teacher.LastName = "Maasikas";
            teacher.HairColor = HairColor.Black;
            teacher.Gender = Gender.Male;
            teacher.SetAge(43);
            teacher.Weight = 76.43;
            teacher.Salary = 1111;
            
            teacher.Introduce();

            Person secretary = new Person();
            secretary.SetFirstName("Jüri");
            secretary.LastName = "Juurikas";
            secretary.HairColor = HairColor.None;
            secretary.Gender = Gender.Male;
            //secretary.SetAge(22);
            secretary.Weight = 36.43;
            secretary.Salary = 3000;

           
            // Massiiv, kuhu saab sisse panna Person tüüpi objekte
            Person[] persons = new Person[3];
            persons[0] = secretary;
            persons[1] = student;
            persons[2] = teacher;


            Person[] workers = new Person[] { secretary, teacher, student };

            // 1. Prindi kõigi nimed välja
            for (int i = 0; i < workers.Length; i++)
            {
                Console.WriteLine(workers[i].FullName);
            }

            short c = 444;
            int d = 21;
            c = (short)d;
            d = c;

            int e = 2;
            double f = 4.99999;
            e = (int)f;
            f = e;
            // 2. Leia keskmine vanus ja prindi välja
            int a = 3;
            int? b = null;
            
            if (b != null)
            {
                a = (int)b;
                Console.WriteLine(a);
            }
            
            b = a;

          

            int? j = 3;
            double h = (double)j;
            
            int? ageSum = 0;

            // Kui ma tahan, et keskmine ei arvestaks isikuid, kelle vanus on seadistamata
            int ageCounter = 0;

            for (int i = 0; i < workers.Length; i++)
            {
                if(workers[i].GetAge() != null)
                {
                    ageSum += workers[i].GetAge();
                    ageCounter++;
                }
            }

            // Double.Parse(null) ei tööta
            // Convert.ToDouble(null) tagastab 0
            // (double)null ka ei tööta

            // Kahe täisarvu jagamisel on tulemus alati täisarv
            // kõik peale koma süüakse ära
            // double tüüpi muutuja sisse pannakse täisarv
            // Selleks, et jagatis oleks reaalarv, pean ühe
            // tehte muutujast tegema reaalrvuks
            double ageAverage = (double)ageSum / ageCounter;
            Console.WriteLine("Vanuste keskmine on {0}", ageAverage);

            // 3. Prindi kõige rohkem kaaluva isiku sünniaasta

            double? weightMax = 0;
            int? birthYear = 0;
            
            for (int i = 0; i < workers.Length; i++)
            {
                if (workers[i].Weight > weightMax)
                {
                    weightMax = workers[i].Weight;
                    birthYear = workers[i].BirthYear;
                }
            }

            Console.WriteLine("Kõige rohkem kaaluva isiku sünniaasta on {0}", birthYear);

            // 3. Prindi kõige rohkem kaaluva isiku sünniaasta, 
            // perekonnanimi, juukservärv ja palk
            weightMax = 0;
            Person personWithMaxWeight = null;

            for (int i = 0; i < workers.Length; i++)
            {
                if (false && workers[i].Weight > weightMax)
                {
                    weightMax = workers[i].Weight;
                    personWithMaxWeight = workers[i];
                }
            }

            if(personWithMaxWeight != null)
            {
                Console.WriteLine("Raskeima isiku sünniaasta on {0}, perekonnamini on {1}, juuksevärv on {2} ja palk on {3}",
                personWithMaxWeight.BirthYear, personWithMaxWeight.LastName, personWithMaxWeight.HairColor,
                personWithMaxWeight.Salary);
            }
            else
            {
                Console.WriteLine("Raskeimat isikut ei õnnestunud leida");
            }

            persons[0].Weight = 50;

            workers[0].Weight = 100;

            var worker = persons[0];

            worker.Weight = 20;

            var newWorker = worker;

            newWorker.Weight = 10;

            Console.WriteLine("Persons massiivi esimese isiku kaal on {0}", persons[0].Weight);

            int x = 5;

            int y = x; // y = 5

            x = 3;

            // Value type muutujad, kui omistada uue muutuja väärtuseks (või lisada kuhugi massiivi)
            // siis tehakse neist koopia
            // int, string, bool, double
            // Reference type muutujad, kui omistada uue muutuja väärtuseks (või lisada kuhugi massiivi)
            // siis on tegu ikka sama elemendiga.
            // kõik objektid ja massiivid



            Console.WriteLine(y);

            int[] numbers = new int[] { x };

            numbers[0] = 8;

            Console.WriteLine(x);

            int[] newNumbers = numbers;
            newNumbers[0] = 7;
            Console.WriteLine(numbers[0]);


            Person dentist = new Person("Kalle", -100);

            Console.ReadLine();
        }
    }
}
