﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{

    // enum on eriline muutuja tüüp, kus saab defineerida
    // mingid konstantsed valikud
    // tegelikult hoitakse väärtust täisarvuna
    // Gender.Male on sama mis 0
    // Gender.Female on sama, mis 1
    enum Gender
    {
        Undefined,
        Male,
        Female
    }

    enum HairColor
    {
        None,
        Grey,
        Black,
        White,
        Brown,
        Red
    }
    
    class Person
    {
        protected string firstName;
        // Nullable ? tähendab, et väärtus võib olla lisaks numbrilisele väärtusele ka null
        // Vaikeväärtus nullable tüüpidel on alati null
        private double? weight;

        private int? age;

        public Gender Gender { get; set; }
        public HairColor HairColor { get; set; }
        public double? Salary { get; set; }
        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return firstName + " " + LastName;
            }
        }

        public int? BirthYear
        {
            get
            {
                if(age != null)
                {
                    return DateTime.Now.Year - age;
                }
                else
                {
                    return null;
                }
            }
        }

        // Property
        public double? Weight
        {
            get
            {
                if (weight == null)
                {
                    Console.WriteLine("VIGA! Kaal on seadistamata");
                }
                return weight;
            }
            set
            {
                // Kui ma annan väärtuse nii student.Weight = 56.45, 
                // siis siit see 56.45 ongi value
                if(value < 0)
                {
                    Console.WriteLine("VIGA! Kaal ei saa olla negatiivne");
                }
                else
                {
                    weight = value;
                }
                
            }
        }

        public Person(string firstName, double weight)
        {
            //this.weight = weight;
            Weight = weight;
        }

        public Person()
        {

        }

     
        // Kapseldamine encapsulation

        public void SetFirstName(string firstName)
        {     
            if(firstName == "" || firstName == null)
            {
                Console.WriteLine("VIGA! Nimi ei saa olla tühi");
            }
            else
            {
                // this.name tähendab, klassi enda muutujat name
                // name on siis SetName paramaater
                this.firstName = firstName;
            }
           
        }

        public void SetAge(int age)
        {
           
            if (age < 0)
            {
                Console.WriteLine("VIGA! Vanus ei saa olla negatiivne");
            }
            else if(age > 130)
            {
                Console.WriteLine("VIGA! Vanus ei saa olla üle 130");
            }
            else
            {
                this.age = age;
            }
        }

        public int? GetAge()
        {
            if (age == null)
            {
                Console.WriteLine("VIGA! Vanus on seadistamata");
            }

            return this.age;
        }
        // public tähendab, et muutuja või meetod on avalik
        // ehk väljaspoole klassi ennast nähtav
        public void Introduce()
        {
            Console.WriteLine("Tere minu nimi on {0}", firstName);
        }

        public static void SayHello()
        {
            Console.WriteLine("Hello");
        }
    }
}
