﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{
    class Color
    {
        public string Name { get; set; }
        public string Code { get; set; }

        // HEX      RGB
        // #ff0000	rgb(255, 0, 0) - punane
        // #00ff00  rgb(0, 255, 0) - roheline
        // #0000ff  rgb(0, 0, 255) - sinine
        // #ffffff  rgb(255, 255, 255) - valge
        // #000000  rgb(0, 0, 0) - must


        public string GetCode()
        {
            return Code;
        }
        public static string GetCodeFromName(string name)
        {
            var code = "";

            //if(name == "Red")
            //{
            //    code = "#ff0000";
            //}
            //else if  (name == "Blue")
            //{
            //    code = "#0000ff";
            //}
            //else if (name == "Black")
            //{
            //    code = "#000000";
            //}
            //else if (name == "White")
            //{
            //    code = "#ffffff";
            //}
            //else if(name == "Green")
            //{

            //    code = "#00ff00";
            //}
            //else
            //{
            //    code = null;
            //}

            // Muudame kõik tähed väikseks
            name = name.ToLower();

            // Muudab kõik tähed suureks
            // name.ToUpper();

            switch (name)
            {
                case "red":
                    code = "#ff0000";
                    break;
                case "blue":
                    code = "#0000ff";
                    break;
                case "black":
                    code = "#000000";
                    break;
                case "white":
                    code = "#ffffff";
                    break;
                    // Kui ma tahan, et sama plokk käivitatakse
                    // mitme erineva väärtuse korral,
                    // siis jätan break ära ning käitivatakse 
                    // järgnev plokk
                case "green":
                case "grüne":
                case "roheline":
                    code = "#00ff00";
                    break;
                // Käitub nagu else
                default:
                    code = null;
                    break;
            }

            return code;
        }
    }
}
