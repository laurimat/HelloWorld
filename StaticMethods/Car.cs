﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{
    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public double Acceleration { get; set; }
        public Country Country { get; set; }
        public Color Color { get; set; }

        //public Car()
        //{

        //}
        // Klassi kirja pannes, kui ma ei defineeri konstrutorit public Car(),
        // siis luuakse see ise varjatult

    }
}
