﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            // new Country() tähendab, et kutsutakse välja
            // Country klassist ilma parameetriteta konstrutor
            // Country();
            Country germany = new Country();
            Country italy = new Country() { Name = "Italy", ShortName = "ITA" };
            
            Country estonia = new Country();

            Country spain = new Country("Spain", "SPN");
            Country finland = new Country("Finland", "FIN");
            Country russia = new Country("Russia", "RUS");
            
            Console.WriteLine(italy.Name);
            Console.WriteLine(finland.Name);
            Console.WriteLine("Country objekte on mul kokku {0}", Country.Counter);

            Console.WriteLine();
            Console.WriteLine("Loodud riikide nimekiri:");

            var countryNames = Country.GetUsedCountryNames();

            foreach (var countryName in countryNames)
            {
                Console.WriteLine(countryName);
            }
            
            Console.WriteLine();

            string a = "kola";
            string b = "maja";
            int compareValue = string.Compare(a, b);
            
            if(compareValue < 0)
            {
                Console.WriteLine("Sõna {0} on eespool kui {1}", a, b);
            }
            else if(compareValue > 0)
            {
                Console.WriteLine("Sõna {0} on tagapool kui {1}", a, b);
            }
            else
            {
                Console.WriteLine("Sõnad {0} ja {1} võrdsed", a, b);
            }

            compareValue = a.CompareTo(b);
            
            if (compareValue < 0)
            {
                Console.WriteLine("Sõna {0} on eespool kui {1}", a, b);
            }
            else if (compareValue > 0)
            {
                Console.WriteLine("Sõna {0} on tagapool kui {1}", a, b);
            }
            else
            {
                Console.WriteLine("Sõnad {0} ja {1} võrdsed", a, b);
            }

            // tagastab -1, kui esimene sõna on tähestikus eespool
            // tagastab 0, kui sõnad on võrdsed
            // tagastab 1, kui esimene sõna on tähestikus tagapool

            string someString = "Kala";

            someString.CompareTo("Maja");

            Console.WriteLine("Sisesta värv, mille värvikoodi soovid teada");
            var colorName = Console.ReadLine();

            string colorCode = Color.GetCodeFromName(colorName);

            Console.WriteLine("Värvi {0} kood on {1}", colorName, colorCode);

            Console.WriteLine();

            var someColor = new Color() { Name = "Black", Code = "#000000" };

            var someColorCode = someColor.GetCode();

            Console.WriteLine("Värvi kood on {0}", someColorCode);

            Console.WriteLine();



            List<Color> colors = new List<Color>();
            colors.Add(new Color() { Code = "#ff0000", Name = "Red" });

            var color = new Color();
            color.Name = "Blue";
            color.Code = "#0000ff";
            colors.Add(color);

            color = new Color() { Code = "#000000", Name = "Black" };
            colors.Add(color);

            colors.Add(new Color() { Code = "#ffffff", Name = "White" });
            colors.Add(new Color() { Code = "#00ff00", Name = "Green" });
            
            var countries = new List<Country>()
            {
                new Country() { Name = "Germany", ShortName = "GER" },
                new Country() { Name = "United States", ShortName = "USA" },
                new Country() { Name = "Japan", ShortName = "JAP" }
            };

            var car = new Car();

            // Mis värvi autot tahad?
            Console.WriteLine("Vali auto värv:");
            // 1 Red
            // 2 Black
            // 3 White
            // 4 Green
            for (int i = 0; i < colors.Count; i++)
            {
                Console.WriteLine("{0} {1}", i + 1, colors[i].Name);

            }
            int selection = Convert.ToInt32(Console.ReadLine());
            car.Color = colors[selection - 1];
           
            // Mis riigi autot tahad?
            for (int i = 0; i < countries.Count; i++)
            {
                Console.WriteLine("{0} {1}", i + 1, countries[i].Name);

            }
            selection = Convert.ToInt32(Console.ReadLine());
            car.Country = countries[selection - 1];

            Console.WriteLine("Valisid {0} värvi riigi {1} auto", car.Color.Name, car.Country.Name);

            Console.ReadLine();
        }

    }
}
