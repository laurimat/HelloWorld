﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMethods
{
    class Country
    {
        // Staatiline klassi muutuja
        // tähendab, et sama muutuja on ühine kõikdel objektidel ja klassil
        public static int Counter = 0;

        private static List<string> countryNames = new List<string>();
        
        // Konstruktor, eriline meetod, mis käivitatakse objekti loomisel klassist
        public Country()
        {
            Console.WriteLine("Objekt Country loodi");

            // Konstruktorit saab kasutada, et anda vaikeväärtused
            Name = "Estonia";
            ShortName = "EST";
            Counter++;
            countryNames.Add(Name);
        }

        // Constructor overloading, konstrutori ülelaadimine
        public Country(string name, string shortName)
        {
            Name = name;
            ShortName = shortName;
            Counter++;
            countryNames.Add(Name);
        }
        

        public string Name { get; set; }
        public string ShortName { get; set; }

        public static List<string> GetUsedCountryNames()
        {
            return countryNames;
        }
    }
}
