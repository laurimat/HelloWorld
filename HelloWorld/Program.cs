﻿using System;
// Määrab, et programm pääseb ligi System nimeruumile
// Paikneb System.dll failis
// Annab programmile võimaluse kasutada kõiki 
// System nimeruumis olevaid klasse ja meetodeid
namespace HelloWorld
// Annab võimaluse grupeerida klasse nii, et sama nimega klass saab olla
// erinevates komponentides (nimeruumides) 
{ 
    class Program
    // Kõik .NET komponendid on jagatud klassidesse, mis koondavad 
    // sarnase funktsionaalsuse ja omadused. Keskenduvad konkreetsele funktsionaalsusele.
    {
        // Meetod ehk funktsioon 
        // Main meetod on eriline, sest sellest alustab programm oma tööd
        static void Main(string[] args)
        {
            // Prindib konsooli aknasse rea
            //Console klassi meetodi WriteLine käivitamine
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.Write("Hello World");
            Console.Write("Hello World");
            Console.Write("Hello World");
            Console.ReadLine();
        }
    }

}


