﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sisesta arv");
            int number = Convert.ToInt32(Console.ReadLine());
          
            if (number > 0)
            {
                Console.WriteLine("Number {0} on positiivne", number);
            }
            else
            {
                if(number == 0)
                {
                    Console.WriteLine("Number on 0");
                }
                else
                {
                    Console.WriteLine("Number {0} on negatiivne", number);
                }
               
            }

            Console.WriteLine();

           
            if (number > 0)
            {
                Console.WriteLine("Number {0} on positiivne", number);
            }
            else if (number == 0)
            {
                Console.WriteLine("Number on 0");
            }
            else
            {
                Console.WriteLine("Number {0} on negatiivne", number);
            }

            Console.WriteLine();
            // Kui arv on suurem kahest prindi "Arv on suurem kahest"
            // Kui arv on suurem neljast, prindi "Arv on suurem neljast"
            // Kui arv on väiksem kümnest, prindi "Arv on väiksem kümnest"
            if (number > 2)
            {
                Console.WriteLine("Arv on suurem kahest");
            }
            if (number > 4)
            {
                Console.WriteLine("Arv on suurem neljast");
            }
            if (number < 10)
            {
                Console.WriteLine("Arv on väiksem kümnest");
            }

            Console.ReadLine();
        }
    }
}
