﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string sentence = "Põdral maja metsa sees väiksest aknast välja vaatab";

            Console.WriteLine("Lause pikkus on {0} sübolit", sentence.Length);

            Console.WriteLine("Lause esimene täht on '{0}'", sentence[0]);


            char charO = 'o';

            string word = String.Format("{0}{1}{2}{3}",charO, sentence[2], sentence[3], sentence[4]);
            Console.WriteLine(word);

            // Sõnaosa leidmine
            // Ühe parameetriga tähendab, et sellest indeksist lõpuni
           

            word = sentence.Substring(7);
            Console.WriteLine("Lause alates kaheksandast sümbolist: '{0}'", word);
            // Kahe parameetriga tähendab, et indeksist kuni niipalju sümboleid
            word = sentence.Substring(0, 5);
            Console.WriteLine("Lause esimesesest viiest tähest koosnev sõna: '{0}'", word);
            word = sentence.Substring(7, 4);
            Console.WriteLine("Nelja täheline sõna kaheksandast sümbolist alates: '{0}'", word);

            // Kuidas leida esimese tühiku indeks
            // Kui kutsume välja ühe parameetriga, siis alustab otsimist algusest
            int spaceIndex = sentence.IndexOf(' ');
            Console.WriteLine("Esimese tühiku asukoha indeks on: '{0}'", spaceIndex);

            int index = sentence.IndexOf("metsa");
            Console.WriteLine("Sõna 'metsa' asukoha esimese tähe indeks on: '{0}'", index);

            int secondSpaceIndex = sentence.IndexOf(' ', spaceIndex + 1);
            Console.WriteLine("Teise tühiku asukoha indeks on: '{0}'", secondSpaceIndex);

            // Prindi välja esimene sõna lausest
            string firstWord = sentence.Substring(0, spaceIndex);

            Console.WriteLine("Lause esimene sõna on '{0}'", firstWord);

            int wordLength = secondSpaceIndex - spaceIndex - 1;
            Console.WriteLine("Teine sõna pikkus on '{0}'", wordLength);

            string secondWord = sentence.Substring(spaceIndex + 1, wordLength); 
            Console.WriteLine("Lause teine sõna on '{0}'", secondWord);

            int lastSpaceIndex = sentence.LastIndexOf(' ');
            string lastWord = sentence.Substring(lastSpaceIndex + 1);
            Console.WriteLine("Lause viimane sõna on '{0}'", lastWord);

            int oneBeforeLastSpaceIndex = sentence.LastIndexOf(' ', lastSpaceIndex - 1);
            wordLength = lastSpaceIndex - oneBeforeLastSpaceIndex - 1;

            string oneBeforeLastWord = sentence.Substring(oneBeforeLastSpaceIndex + 1, wordLength);
            Console.WriteLine("Lause eelviimane sõna on '{0}'", oneBeforeLastWord);

            word = GetFirstWordWithLength("Läbi lume sahiseva sõidab", 6);
            Console.WriteLine("Esimene neljast tähest koosnev sõna on '{0}'", word);

            Console.ReadLine();
        }

        static string GetFirstWordWithLengthOld(string sentence, int length)
        {
            string word = "";

            int lastSpaceIndex = 0;

            while(lastSpaceIndex != -1)
            {
                lastSpaceIndex = sentence.IndexOf(' ', lastSpaceIndex);
            }

            // Kontrollin, kas üldse on tühikuid või koosneb ühest sõnast
            lastSpaceIndex = sentence.IndexOf(' ', lastSpaceIndex);
            if(lastSpaceIndex == -1)
            {
                if(sentence.Length == length)
                {
                    return sentence;
                }
            }
            else
            {
                string firstWord = sentence.Substring(0, lastSpaceIndex);
                if(firstWord.Length == length)
                {
                    return firstWord;
                }

                int secondSpaceIndex = sentence.IndexOf(' ', lastSpaceIndex + 1);

                string secondWord;

                if (secondSpaceIndex == -1)
                {
                    secondWord = sentence.Substring(lastSpaceIndex + 1);
                    if (secondWord.Length == length)
                    {
                        return secondWord;
                    }
                }
                else
                {
                    int wordLength = secondSpaceIndex - lastSpaceIndex - 1;

                    lastSpaceIndex = secondSpaceIndex;

                    secondWord = sentence.Substring(lastSpaceIndex + 1, wordLength);
                    if (secondWord.Length == length)
                    {
                        return secondWord;
                    }
                    
                }
              
            }

            return word;
        }

        static string GetFirstWordWithLength(string sentence, int length)
        {
            string word = "";

            int lastSpaceIndex = 0;

            while (lastSpaceIndex != -1)
            {
                int currentSpaceIndex;
                // Ainult esimene kord
                if (lastSpaceIndex == 0)
                {
                    currentSpaceIndex = sentence.IndexOf(' ', 0);
                }
                // Kõik järgnevad korrad
                else
                {
                    currentSpaceIndex = sentence.IndexOf(' ', lastSpaceIndex + 1);
                }
                 
                // Järgmist tühikut ei ole
                if (currentSpaceIndex == -1)
                {
                    // Vaatan, kas viimasest tühikust lõpuni moodustatud sõna
                    // on nii pikk, kui oli nõutud
                    
                    // Ainult esimene kord
                    if (lastSpaceIndex == 0)
                    {
                        word = sentence.Substring(lastSpaceIndex);
                    }
                    // Kõik järgnevad korrad
                    else
                    {
                        word = sentence.Substring(lastSpaceIndex + 1);
                    }
                       
                    if (word.Length == length)
                    {
                        return word;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    int wordLength;
                    // Ainult esimene kord
                    if (lastSpaceIndex == 0)
                    {
                        wordLength = currentSpaceIndex - lastSpaceIndex;
                        word = sentence.Substring(lastSpaceIndex, wordLength);
                    }
                    // Kõik järgnevad korrad
                    else
                    {
                        wordLength = currentSpaceIndex - lastSpaceIndex - 1;
                        word = sentence.Substring(lastSpaceIndex + 1, wordLength);
                    }
                    
                    if (word.Length == length)
                    {
                        return word;
                    }
                    lastSpaceIndex = currentSpaceIndex;
                }   
            }
            return word;
        }

        static string FirstElement(string[] words)
        {
           
            return "";
        }
    }
}
