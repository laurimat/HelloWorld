﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Meetodi ülekirjutamine
// Method overriding
// Kui vanemklassis (Animal) on mingi meetod, mis võiks toimida teisiti
// pärinevates klassides (Cat),
// siis kirjutame pärinevs klassis selle meetodi sisu üle
namespace Overriding
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat persian = new Cat();
            Dog tax = new Dog();
            Animal animal = new Animal();

            persian.Eat();
            tax.Eat();
            animal.Eat();

            persian.Drink();
            tax.Drink();
            animal.Drink();

            // Kirjuta üle meetod Sleep() kõigis 
            // vahepealsetes klassides, millest pärineb hunt
            // Predator sleeps
            // Wildanimal sleeps

            WildAnimal wildAnimal = new WildAnimal();
            Predator predator = new Predator();
            Wolf wolf = new Wolf();
            
            wolf.Sleep();
            predator.Sleep();
            wildAnimal.Sleep();
                       
            Cat siam = new Cat("Garfield", 2, true, 6);
            Pet pet = new Pet("Kalle", 10, true);

            Console.WriteLine("Siiami nimi on {0}", siam.Name);

            Dog labrador = new Dog();

            DomesticAnimal domesticAnimal = new DomesticAnimal("Eduard", 100);
            Console.WriteLine("Domesticanimal nimi on {0}", domesticAnimal.Name);

            Console.ReadLine();

        }
    }
}
