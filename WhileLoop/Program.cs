﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // WHILE TSÜKLIT KASUTATAKSE SIIS,
            // KUI ME EI TEA ENNE TSÜKLISSE JÕUDMIST
            // MITU KORDA ME TSÜKLIT KORDAME/LÄBIME

            while(true)
            {
                // Peata programmi töö 1 sekundiks
                // 1000ms = 1s
                // Thread.Sleep(1000);
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Console.WriteLine("Hello");
                break;
            }

            int a = 300;
            while (a == 300)
            {
                // Peata programmi töö 1 sekundiks
                // 1000ms = 1s
                // Thread.Sleep(1000);
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Console.WriteLine("Hello Cosmos");
                a = 4;
            }

            // Prindi 3 korda ekraanile "Mida iganes"

            int counter = 0;
            while(counter < 3)
            {
                counter++;
                Console.WriteLine("Mida iganes {0} korda", counter);
            }

            bool doContinue = true;
            
            while(doContinue)
            {
                Console.WriteLine("Sisesta postitiivne number");
                var number = Convert.ToInt32(Console.ReadLine());
                if (number <= 0)
                {
                    doContinue = false;
                }
            }


            Console.ReadLine();

        }
    }
}
