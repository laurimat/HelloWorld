﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 3;

            // Täisarvude massiivi loomine.
            // 3 näitab, et massiivi mahub 3 täisarvu
            int[] numbers = new int[3];

            // Täisarvu massiivi luues täiedatkase see 0'dega.
            Console.WriteLine(numbers[2]);

            // Lisan massivi esimesele kohale numbri 3
            numbers[0] = 3;
            numbers[1] = -4;
            numbers[2] = 17;

            Console.WriteLine(numbers[0]);
            Console.WriteLine(numbers[1]);
            Console.WriteLine(numbers[2]);

            Console.WriteLine();

            numbers[0] = -23;
            numbers[1] = 0;
            numbers[2] = 3;

            Console.WriteLine(numbers[0]);
            Console.WriteLine(numbers[1]);
            Console.WriteLine(numbers[2]);

            Console.WriteLine();

            // Suurendame igat elementi 2 võrra

            int b = 3;
            b += 2;

            numbers[0] += 2;
            numbers[1] += 2;
            numbers[2] += 2;
            numbers[3] += 2;

            Console.WriteLine(numbers[0]);
            Console.WriteLine(numbers[1]);
            Console.WriteLine(numbers[2]);

            Console.WriteLine();

            int sum = numbers[0] + numbers[1] + numbers[2];
            Console.WriteLine("Arvude {0}, {1} ja {2} summa on {3}"
                , numbers[0], numbers[1], numbers[2], sum);


            // Suurenda esimest numbrit massiivis teise ja kolmanda numbri korrutise võrra
            // Prindi see esimene number siis välja

            // -21 2 5
            numbers[0] = numbers[0] + numbers[1] * numbers[2];

            Console.WriteLine(numbers[0]);

            numbers[0] += numbers[1] * numbers[2];

            Console.WriteLine(numbers[0]);
            Console.ReadLine();


        }
    }
}
