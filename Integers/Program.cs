﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integers
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 2;

            number = 4;

            int a = 2;
            int b = 3;

            int sum = a + b;

            // Arvude 2 ja 3 summa on 5
            Console.WriteLine("Arvude {0} ja {1} summa on {2}", a, b, sum);
            Console.WriteLine("Arvude " + a + " ja " + b + " summa on " + (a + b));

            long c = 24000;
            int d = 23473;
            short e = 32000;

            // Implicit convert, automaatne teisendamine
            // Toimub automaatselt siis, kui teisendatav number on tüübilt väiksem, kui
            // see number, milleks teisendame näitks int -> short
            int f = e;

            // Explicit convert
            // Peate veenduma, et see number mahub ära
            short g = (short)d;

            int h = -32769;

            short i = (short)h;
            Console.WriteLine(i);
                       
            h = (int)c;
            Console.WriteLine(h);

            long j = 3000000000;

            h = (int)j;
            Console.WriteLine(h);


            Console.ReadLine();
        }
    }
}
