﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[4];
            string[] words = new string[4];

            Console.WriteLine("Kolmandal kohal sõna '{0}'", words[2]);

            words[0] = "";

            string word = "Kala";
            string name;
            // null tähendab, et väärtus puudub
            word = null; 
            word = "Maja";
            word = null;

            words[0] = null;
            words[0] = "Auto";
            words[0] = null;
            words[0] = "";

            words[0] = "Põdral";
            words[1] = "maja";
            words[2] = "metsa";
            words[3] = "sees";

            
            // words.Legth näitab massivi elementi arv 
            for (int i = 0; i < words.Length; i++)
            {
                Console.WriteLine(words[i]);
            }

            string sentence = "";
            
            // Tee nendest sõnadest lause
            for (int i = 0; i < words.Length; i++)
            {
                sentence += words[i];
                if (i < words.Length - 1)
                {
                    sentence += " ";
                }
            }

            Console.WriteLine("Lause on: '{0}'", sentence);

            sentence = string.Join(" ", words);
            Console.WriteLine("Uus lause on: '{0}'", sentence);

            string[] newWords = sentence.Split(' ');

            for (int i = 0; i < newWords.Length; i++)
            {
                Console.WriteLine(newWords[i]);
            }

            // Küsi kasutajalt lause ning prindi 
            // välja selles lauses olevad sõnad,
            // iga sõna eraldi reale
            Console.WriteLine("Sisesta lause");
            sentence = Console.ReadLine();
            string[] sentenceWords = sentence.Split(' ');

            for (int i = 0; i < sentenceWords.Length; i++)
            {
                Console.WriteLine(sentenceWords[i]);
            }

            // Massiivi loomine ühe reaga, 
            // pannes sinna kohe väärtused
            numbers = new int[] { 1, 3, -1, 10, 24, 23 };

            string[] months = new string[] { "Jaanuar", "Veebruar", "Märts" };

            char[] symbols = new char[] { ',', ';', '-', '.' };

            symbols[2] = '?';

            
            bool[] booleans = new bool[] { true, false, true };
            booleans[1] = true;


            double[] doubles = new double[] { 2, 2.3, 43, 2.3333 };
            doubles[2] = 0.005;

            Console.ReadLine();
        }
    }
}
