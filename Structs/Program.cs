﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structs
{
    class Program
    {
        static void Main(string[] args)
        {
            Car a = new Car() { Model = "Audi", Year = 2017 };

            Car b = a;
            
            Console.WriteLine("b auto aasta on {0} ja mudel on {1}", b.Year, b.Model);

            Console.WriteLine();

            b.Year = 2000;
            b.Model = "Bmw";

            Console.WriteLine("b auto aasta on {0} ja mudel on {1}", b.Year, b.Model);
            Console.WriteLine("a auto aasta on {0} ja mudel on {1}", a.Year,a.Model);

            Car c;

            Console.ReadLine();
        }
    }
}
