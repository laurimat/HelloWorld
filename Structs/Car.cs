﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structs
{
    struct Car
    {
        public int Year { get; set; }
        public string Model { get; set; }
    }
}
