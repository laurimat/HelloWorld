﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booleans
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 3;

            var b = a < 8;
            Console.WriteLine(b);

            bool c = a <= 3;
            Console.WriteLine(c);

            var d = a > 4 || a < 2 || a > 1;
            Console.WriteLine(d);

            int f = 5;
            var e = (a > 4 && f > 4) || (a < 4 && f < 6) || f > 8;
            Console.WriteLine(e);

            if ((a > 4 && f > 4) || (a < 4 && f < 6) || f > 8)
            {
                Console.WriteLine("Is true");
            }
            // If kontroolib alati, kas seal sulgudes olev
            // tingimus on tõene, ehk true
            // lisa kontrolli == true, ei ole vaja kirja panna
            if(e)
            {
                Console.WriteLine("Is true");
            }

            if(a != 4)
            {
                Console.WriteLine("a != 4");
            }

            var g = !true;
            Console.WriteLine(g);

            bool h = !(a < 8);
            Console.WriteLine(h);

            if (((a > 4) && f > 4) || !(a < 4 && !(f < 6)) || f > 8)
            {
                Console.WriteLine("Is true");
            }
            else
            {
                Console.WriteLine("Is false");
            }

            int i = 11;

            // Koosta if kontroll nii, et kontrollid, kas arv ei jagu 5ga
            if(i % 5 != 0)
            {
                Console.WriteLine("Arv ei jagu 5'ga");
            }

            
            Console.ReadLine();
        }
    }
}
