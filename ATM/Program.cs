﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace ATM
{
    class Program
    {
        static void Main(string[] args)
        {
            double balance = LoadBalance();

            string realPin = LoadPin();
            string enteredPin = "";

            Console.OutputEncoding = Encoding.UTF8;
           
            Console.WriteLine("Sisesta pin");
            enteredPin = Console.ReadLine();

            bool pinVerified = false;


            if (enteredPin == realPin)
            {
                pinVerified = true;
            }
            else
            {
                int counter = 0;
                while (!pinVerified && counter < 2)
                {
                    counter++;
                    Console.WriteLine("Vale pin. Proovi uuesti.");
                    Console.WriteLine("Sul on {0} katset veel", 3 - counter);
                    enteredPin = Console.ReadLine();
                    if (enteredPin == realPin)
                    {
                        pinVerified = true;
                    }
                }
            }


            if (!pinVerified)
            {
                Console.WriteLine("Panid 3 korda vale pin koodi. Nämm nämm nämm");
            }
            else
            {
                Console.WriteLine("Vali tehing");
                Console.WriteLine("a) Sularaha välja");
                Console.WriteLine("b) Sularaha sisse");
                Console.WriteLine("c) Kontojääk");
                Console.WriteLine("d) Konto väljavõte");
                Console.WriteLine("e) Vaheta pin");
                Console.WriteLine("f) Välju");
                                
                string answer = Console.ReadLine();
                if(answer == "a")
                {
                    Console.WriteLine("Vali summa");
                    Console.WriteLine("a) 5€");
                    Console.WriteLine("b) 10€");
                    Console.WriteLine("c) 20€");
                    Console.WriteLine("d) 50€");
                    Console.WriteLine("e) 100€");
                    Console.WriteLine("f) Muu");
                    Console.WriteLine("g) Väikesed kupüürid");

                    answer = Console.ReadLine();

                    int amount = 0;

                    if (answer == "a")
                    {
                        amount = 5;
                    }
                    else if (answer == "b")
                    {
                        amount = 10;
                    }
                    else if (answer == "c")
                    {
                        amount = 20;
                    }
                    else if (answer == "d")
                    {
                        amount = 50;
                    }
                    else if (answer == "e")
                    {
                        amount = 100;
                    }
                    else if(answer == "f")
                    {
                        Console.WriteLine("Sisesta summa");
                        amount = int.Parse(Console.ReadLine());
                    }
                   
                    if (balance >= amount)
                    {
                        balance -= amount;

                        SaveBalance(balance);
                        SaveTransaction("Sularaha väljavõtt", amount);

                        Console.WriteLine("Siin on sinu {0}", amount);
                    }
                    else
                    {
                        Console.WriteLine("Kontol pole piisavalt vahendeid. Tagastan kaardi");
                    }
                }
                if (answer == "e")
                {
                    Console.WriteLine("Sisesta praegune pin kood");
                    answer = Console.ReadLine();
                    if(answer == realPin)
                    {
                        Console.WriteLine("Sisesta uus pin kood");
                        string firstPin = Console.ReadLine();

                        Console.WriteLine("Sisesta uus pin kood uuesti");
                        string secondPin = Console.ReadLine();

                        if(firstPin == secondPin)
                        {
                            realPin = secondPin;
                            SavePin(realPin);
                            Console.WriteLine("Pin kood edukalt muudetud");
                        }
                        else
                        {
                            Console.WriteLine("Sisestatud pin koodid on erinevad");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Sisestatud pin oli vale");
                        Console.WriteLine("Tagastan kaardi");
                    }
                }

            }
            Console.ReadLine();   
        }

        static int GenerateCashAmountNotEffective()
        {
            int amount = 0;

            int counter = 0;
            //5...500
            do
            {
                Random random = new Random();
                amount = random.Next(5, 501);
                counter++;
                Console.WriteLine("Random number {0} {1}", amount, counter);
            }
            while (amount % 5 != 0);
         
            return amount;
        }

        static int GenerateCashAmount()
        {
            int amount = 0;
            Random random = new Random();
            amount = random.Next(1, 101) * 5;
            return amount;
        }

        static void SaveTransaction(string text, double balance)
        {
            FileStream stream = new FileStream("transactions.txt", FileMode.Append, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);

            writer.WriteLine("{0}: {1}€", text, balance);

            writer.Close();
            stream.Close();
        }

        static void SaveBalance(double balance)
        {
            FileStream stream = new FileStream("balance.txt", FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);

            writer.WriteLine(balance);

            writer.Close();
            stream.Close();
        }

        static double LoadBalance()
        {
            double balance = 0;

            FileStream stream = new FileStream("balance.txt", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(stream, Encoding.Default);

            //balance = double.Parse(reader.ReadLine());
            balance = Convert.ToDouble(reader.ReadLine());

            reader.Close();
            stream.Close();

            return balance;
        }

        static void SavePin(string pin)
        {
            FileStream stream = new FileStream("pin.txt", FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);

            writer.WriteLine(pin);

            writer.Close();
            stream.Close();
        }

        static string LoadPin()
        {
            string pin = "";

            FileStream stream = new FileStream("pin.txt", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(stream, Encoding.Default);
            
            pin = reader.ReadLine();

            reader.Close();
            stream.Close();

            return pin;
        }

        static void PrintCoinFlipCounts()
        {
            long countFlip = 0;
            long countFlop = 0;

            long counter = 0;

            Random random = new Random();

            while (counter < 50000000)
            {
               
                int number = random.Next(0, 2);
                if (number == 0)
                {
                    countFlip++;
                }
                else
                {
                    countFlop++;
                }

                counter++;
            }

            Console.WriteLine("Kulli saadi: {0} korda", countFlip);
            Console.WriteLine("Kirja saadi: {0} korda", countFlop);
        }
    }
}
