﻿using System;
using System.Text;
using System.IO;

namespace WriteToFile
{
    class Program
    {
        static void Main(string[] args)
        {

            // FileMode.CreateNew kasuta siis, kui tahad, et annaks veateate, kui sama nimega fail on juba olemas
            // Kui tahad, et üle kirjutaks faili sisu igakord, kasuta FileMode.Create
            // FileMode.Append kirjutab igakord olemasolevale failile juurde
            FileStream stream = new FileStream("output.txt", FileMode.Create, FileAccess.Write);

            // Voogu kirjutaja
            // Teine parameeter võimaldab määrata kodeeringu
            StreamWriter writer = new StreamWriter(stream, Encoding.Default);

            writer.WriteLine("Hello world öäüõ€");

            // Windowsis reavahetus \r\n
            // Unix ja Mac \n
           
            writer.Write("Hello\r\nworld\r\nöäüõ€");
            writer.WriteLine("Hello world öäüõ€");

            // Kirjuta kõik sõnad sõnade massiviist faili
            string[] words = new string[] { "Jaanuar", "Veebruar", "Märts", "Aprill" };

            for (int i = 0; i < words.Length; i++)
            {
                writer.WriteLine(words[i]);
            }

            // Oluline sulgeda voogu kirjuta ja seejärel voog ise
            writer.Close();
            stream.Close();

            File.WriteAllText("output2.txt", "Tere kuidas läheb\r\n");
            File.AppendAllText("output2.txt", "Lisatav tekst\r\n");
            File.AppendAllLines("output2.txt", words);
            File.AppendAllText("output2.txt", "Lisatav tekst\r\n");
            

            Console.ReadLine();
        }
    }
}
